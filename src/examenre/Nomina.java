/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenre;

/**
 *
 * @author Javier Duran
 */
public class Nomina {
    
    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int dias;
    
    public Nomina(){
        this.numRecibo = 0;
        this.nombre = "";
        this.puesto = 0;
        this.nivel = 0;
        this.dias = 0;     
    }
    
    public Nomina(int numRecibo,String nombre,int puesto,int nivel,int dias){
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.dias = dias;   
    }
    
    public Nomina(Nomina otro){
        this.numRecibo = otro.numRecibo;
        this.nombre = otro.nombre;
        this.puesto = otro.puesto;
        this.nivel = otro.nivel;
        this.dias = otro.dias;   
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }
    
    public float calcularPago (){
        float pago = 0;
        
        switch (puesto){
            case 1: pago = 100*dias;break;
            case 2: pago = 200*dias;break;
            case 3: pago = 300*dias;break;
        }
        return pago;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        
        switch (nivel){
            case 1: impuesto = calcularPago()*.05f;break;
            case 2: impuesto = calcularPago()*.03f;break;
        }
        return impuesto;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = calcularPago() - calcularImpuesto();
        return total;
    }
    
}
